exports.checkEnableTime = function (startTime, endTime, today) {
    if(today < startTime){
        return false
    }
    if(today > startTime){
        return true
    }
    if(today > endTime) {
        return false
    }
    return true
}