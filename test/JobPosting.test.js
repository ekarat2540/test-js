const assert = require('assert')
const { checkEnableTime } = require('../Jobposting')

describe('JobPosting', function () {
  it('should return fasle when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น' , function() {
    //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,1,30)
    const expectedResult = false

    //Act
    const actualResult = false;

    //Assert
    assert.strictEqual(actualResult,expectedResult)
  })
  it('เวลาเท่ากับเวลาเริ่มต้น' , function() {
    //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,1,31)
    const expectedResult = true
    
    //Act
    const actualResult = checkEnableTime(startTime,endTime,today);

    //Assert
    assert.strictEqual(actualResult,expectedResult)
  })
  it('เวลาระหว่างเวลาเริ่มต้นและเวลาจบ' , function() {
    //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,2,1)
    const expectedResult = false
    
    //Act
    const actualResult = checkEnableTime(startTime,endTime,today);

    //Assert
    assert.strictEqual(actualResult,expectedResult)
  })
  it('เวลาหลังเวลาจบ' , function() {
    //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,2,6)
    const expectedResult = false
    
    //Act
    const actualResult = checkEnableTime(startTime,endTime,today);

    //Assert
    assert.strictEqual(actualResult,expectedResult)
  })	
})

// const { checkEnableTime } = require('../Jobposting')
// // Arrage
// const startTime = new Date(2021, 1, 31)
// const endTime = new Date(2021, 2, 5)
// const today = new Date(2021, 2, 3)
// const expectedResult = true
// // Act
// const actualResult = checkEnableTime(startTime, endTime, today)
// // Assert
// if (actualResult === expectedResult) {
//   console.log('เวลาอยู่ระหว่างเริ่มต้นและสิ้นสุด Passed')
// } else {
//   console.log('เวลาอยู่ระหว่างเริ่มต้นและสิ้นสุด Fail')
// }
